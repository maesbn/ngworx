NG-Worx
===
A tool to calculate the dates when to payout the salaries and bonuses. PHP 5.3 is required.


Used libraries & frameworks
---
None. To show my coding skills I've decided not to use any libraries or frameworks. For testing I did use PHPSpec.

How to run
---
Execute the following command from this directory

```
./bin/ngworx
```

Make sure ./bin/ngworx is executable.

###Options
- start_dated (dd-mm-yyyy): The date from where the application should start
- output_file (string): The file where to write the result to

###Example

```
./bin/ngworx start_dated=01-01-2015 output_file=result_2015.csv
```

Tests
---
PHPSpec where used for testing. To avoid overhead, integration tests where done in the specs (Normally these should been done with Behat for example).
 
```
/* Install dependencies */
./bin/composer install

/* Run spec tests */
./vendor/bin/phpspec run
```

Now what?
---
Because this was not meant to be an highly scalable super fancy production-ready application, I did not include tests. When we want to make this a production ready application we should consider the following:

- add logging
- add debug_mode  
- add other writers
- add i/o
- add deployment script/tool
- add integration tests 
- add continuous integration
- ... 