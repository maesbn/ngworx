<?php

namespace spec\NGWorx\Service\Writer;

use PhpSpec\Exception\Example\FailureException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use NGWorx\Service\Writer\Console;

/**
 *  THIS SHOULD BE AN INTEGRATION TEST
 *
 * Class ConsoleSpec
 * @package spec\NGWorx\Service\Writer
 */
class ConsoleSpec extends ObjectBehavior
{
    /**
     * @var string
     */
    protected $testString = 'Test String';

    /**
     * Test if is initializable
     */
    function it_is_initializable()
    {
        $this->shouldHaveType(Console::class);
    }

    function it_should_write_error($output)
    {
        ob_start();
        $this->writeError($this->testString);
        $output = ob_get_clean();

        if (!strpos($output, $this->testString))
        {
            throw new FailureException('Message was not correctly show. Expected : ' .
                $this->testString . ' got: ' . $output);

            return false;
        }
    }

    function it_should_write_success()
    {
        ob_start();
        $this->writeSuccess($this->testString);
        $output = ob_get_clean();

        if (!strpos($output, $this->testString))
        {
            throw new FailureException('Message was not correctly show. Expected : ' .
                $this->testString . ' got: ' . $output);

            return false;
        }
    }

    function it_should_write_info()
    {
        ob_start();
        $this->writeInfo($this->testString);
        $output = ob_get_clean();

        if (!strpos($output, $this->testString))
        {
            throw new FailureException('Message was not correctly show. Expected : ' .
                $this->testString . ' got: ' . $output);

            return false;
        }
    }
}
