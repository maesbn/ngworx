<?php

namespace spec\NGWorx\Service\Writer;

use NGWorx\Service\Writer\CSV;
use PhpSpec\Exception\Example\FailureException;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

/**
 * THIS SHOULD BE AN INTEGRATION TEST
 *
 * Class CSVSpec
 * @package spec\NGWorx\Service\Writer
 */
class CSVSpec extends ObjectBehavior
{
    /**
     * @var string
     */
    protected $fileName = 'test.csv';

    /**
     * Run constructor
     */
    function let()
    {
        $this->beConstructedWith($this->fileName);
    }

    /**
     * Test if CSV is initializable
     */
    function it_is_initializable()
    {
        $this->shouldHaveType(CSV::class);
    }

    /**
     * Test if CSV can write to file
     */
    function it_can_write_a_csv_file()
    {
        $this->setFirstLine(array('first', 'line'));
        $this->setData(array(array('test line 1', 'data line 1'), array('test line 2', 'data line 2')));
        $this->write();

        if (!is_readable($this->fileName))
        {
            throw new FailureException('File was not written.');
        }

        $content = file_get_contents($this->fileName);
        $check = "first,line\n\"test line 1\",\"data line 1\"\n\"test line 2\",\"data line 2\"\n";

        if ($content != $check)
        {
            throw new FailureException('File content was not correct. Excpected: ' . $content . ' got: ' . $check);
        }

        unlink($this->fileName);
    }
}
