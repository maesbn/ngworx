<?php

namespace spec\NGWorx\Service\Calculator;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use NGWorx\Service\Calculator\Payout;

/**
 * Class PayoutSpec
 * @package spec\NGWorx\Service\Calculator
 */
class PayoutSpec extends ObjectBehavior
{
    /**
     * Run constructor
     */
    function let()
    {
        $this->beConstructedWith(new \DateTime('01-12-2015'));
    }

    /**
     * Test if is initializable
     */
    function it_is_initializable()
    {
        $this->shouldHaveType(Payout::class);
    }

    /**
     * Test if it can calculate dates
     */
    function it_should_calculate_all()
    {
        $this->calculateAll();
        $this->getResult()->shouldBeArray();
        $this->getResult()->shouldHaveCount(1);
        $this->getResult()->shouldHaveKey(12);
        $this->getResult()[12]->shouldHaveKey('bonusday');
        $this->getResult()[12]->shouldHaveKey('payday');
        $this->getResult()[12]->shouldHaveKey('month');
        $this->getResult()[12]['bonusday']->shouldBeLike(new \DateTime('15-01-2016'));
        $this->getResult()[12]['payday']->shouldBeLike(new \DateTime('31-12-2015'));
        $this->getResult()[12]['month']->shouldBe('December');
    }
}
