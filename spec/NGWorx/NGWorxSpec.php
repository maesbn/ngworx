<?php

namespace spec\NGWorx;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use NGWorx\NGWorx;
use NGWorx\Service\Calculator\Payout;
use NGWorx\Service\Writer\CSV;
use NGWorx\Service\Writer\Console;

/**
 * Class NGWorxSpec
 * @package spec\NGWorx
 */
class NGWorxSpec extends ObjectBehavior
{
    /**
     * Run constructor
     */
    function let(Payout $calculator, CSV $writer, Console $consoleWriter)
    {
        $this->beConstructedWith($calculator, $writer, $consoleWriter);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType(NGWorx::class);
    }

    function it_should_run(Payout $calculator, CSV $writer, Console $consoleWriter)
    {
        $calculator->calculateAll()->willReturn(array())->shouldBeCalled();

        $writer->write()->willReturn(true)->shouldBeCalled();
        $writer->setFirstLine(array("Month", "Payday", "Bonus"))->shouldBeCalled();
        $writer->setData(array())->shouldBeCalled();
        $writer->getOutputFile()->willReturn('successFile')->shouldBeCalled();

        $consoleWriter->writeSuccess("Successfully written to successFile")->shouldBeCalled();

        $this->run()->shouldBe(true);
    }
}
