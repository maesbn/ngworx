<?php

namespace NGWorx;

use \NGWorx\Service\Calculator\Payout;
use \NGWorx\Service\Writer\Console;
use  \NGWorx\Service\Writer\CSV;

/**
 * Class NGWorx
 * @package NGWorx
 */
class NGWorx
{
    /**
     * @var Payout
     */
    protected $calculator;

    /**
     * @var CSV
     */
    protected $writer;

    /**
     * @var Console
     */
    protected $consoleWriter;

    /**
     * NGWorx constructor
     *
     * @param Payout $calculator
     * @param CSV $writer
     * @param Console $consoleWriter
     */
    public function __construct(Payout $calculator, CSV $writer, Console $consoleWriter)
    {
        $this->calculator    = $calculator;
        $this->writer        = $writer;
        $this->consoleWriter = $consoleWriter;
    }

    /**
     * Run the calculations
     *
     * @return bool
     */
    public function run()
    {
        $result         = $this->calculator->calculateAll();
        $writableResult = $this->parseForCSV($result);

        $this->writer->setFirstLine(array('Month', 'Payday', 'Bonus'));
        $this->writer->setData($writableResult);

        if (!$this->writer->write())
        {
            $this->consoleWriter->writeError('Unable to write to ' . $this->writer->getOutputFile());

            return false;
        }
        $this->consoleWriter->writeSuccess('Successfully written to ' . $this->writer->getOutputFile());

        return true;
    }

    /**
     * Parse the data to be written to a CSV
     *
     * @param $result
     * @return array
     */
    protected function parseForCSV($result)
    {
        $writableResult = array();

        foreach ($result as $dates)
        {
            $line   = array();
            $line[] = $dates['month'];
            $line[] = $dates['payday']->format('d-m-Y');
            $line[] = $dates['bonusday']->format('d-m-Y');

            $writableResult[] = $line;
        }

        return $writableResult;
    }
}