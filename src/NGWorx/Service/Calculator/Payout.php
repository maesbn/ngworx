<?php

namespace NGWorx\Service\Calculator;

/**
 * Class Payout
 *
 * @package NGWorx\Service\Calculator
 */
class Payout
{
    /**
     * @var \DateTime
     */
    protected $startFrom;

    /**
     * @var array
     */
    protected $result;

    /**
     * Payout constructor.
     * @param \DateTime $startFrom
     */
    public function __construct(\DateTime $startFrom)
    {
        $this->startFrom = $startFrom;
    }

    /**
     * @return \DateTime
     */
    public function getStartFrom()
    {
        return $this->startFrom;
    }

    /**
     * @param \DateTime $startFrom
     */
    public function setStartFrom($startFrom)
    {
        $this->startFrom = $startFrom;
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * Calculate paydays & bonus days starting from startFrom.
     * @return array
     */
    public function calculateAll()
    {
        $date         = $this->startFrom;
        $currentMonth = $date->format('n');

        for ($i = $currentMonth; $i <= 12; $i++)
        {
            $this->result[$i] = array();
            $this->calculateBonusDay(clone $date);
            $this->calculatePayday(clone $date);

            $this->result[$i]['month'] = $date->format('F');
            $date->modify('next month');
        }

        return $this->result;
    }

    /**
     * Calculate bonus date
     *
     * @param $date
     * @return mixed
     */
    protected function calculateBonusDay($date)
    {
        $month = $date->format('n');

        $date->modify('first day of next month');
        $date->modify('+14 days');

        $weekday = $date->format('N');

        if ($weekday > 5)
        {
            $date->modify('next wednesday');
        }

        $this->result[$month]['bonusday'] = $date;

        return $date;
    }

    /**
     * Calculate payday
     *
     * @param $date
     * @return mixed
     */
    protected function calculatePayday($date)
    {
        $month = $date->format('n');

        $date->modify('last day of this month');
        $weekday = $date->format('N');

        if ($weekday > 5)
        {
            $date->modify('next monday');
        }

        $this->result[$month]['payday'] = $date;

        return $date;
    }
}