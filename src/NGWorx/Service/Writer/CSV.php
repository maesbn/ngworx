<?php

namespace NGWorx\Service\Writer;

/**
 * Class CSV
 * @package NGWorx\Service\Writer
 */
class CSV
{
    /**
     * @var array
     */
    protected $data;

    /**
     * @var array
     */
    protected $firstLine;

    /**
     * @var string
     */
    protected $outputFile;

    /**
     * CSV constructor.
     * @param string $outputFile
     */
    public function __construct($outputFile)
    {
        $this->outputFile = $outputFile;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data)
    {
        $this->data = $data;
    }

    /**
     * @return mixed
     */
    public function getOutputFile()
    {
        return $this->outputFile;
    }

    /**
     * @param mixed $outputFile
     */
    public function setOutputFile($outputFile)
    {
        $this->outputFile = $outputFile;
    }

    /**
     * @return mixed
     */
    public function getFirstLine()
    {
        return $this->firstLine;
    }

    /**
     * @param mixed $firstLine
     */
    public function setFirstLine($firstLine)
    {
        $this->firstLine = $firstLine;
    }

    /**
     * Write data to a csv file
     *
     * @return bool
     */
    public function write()
    {
        try
        {
            if (file_exists($this->outputFile) && !is_writable($this->outputFile))
            {
                throw new \Exception('Unable to open file.');
            }

            $fh = fopen($this->outputFile, 'w+');

            fputcsv($fh, $this->firstLine);

            foreach ($this->data as $row)
            {
                fputcsv($fh, $row);
            }

            fclose($fh);
        } catch (\Exception $e)
        {
            return false;
        }

        return true;
    }
}