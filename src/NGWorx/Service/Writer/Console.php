<?php

namespace NGWorx\Service\Writer;

/**
 * Class Console
 *
 * @package NGWorx\Service\Writer
 */
class Console
{
    /**
     * Echo a line in red
     *
     * @param $msg
     */
    public function writeError($msg)
    {
        echo "\033[31m$msg\033[0m\r\n";
    }

    /**
     * Echo a line in yellow
     *
     * @param $msg
     */
    public function writeInfo($msg)
    {
        echo "\033[0;33m$msg\033[0m\r\n";
    }

    /**
     * Echo a line in green
     *
     * @param $msg
     */
    public function writeSuccess($msg)
    {
        echo "\033[0;32m$msg\033[0m\r\n";
    }
}